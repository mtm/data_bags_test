#
# Cookbook Name:: data_bags_test
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.
passwords = Chef::EncryptedDataBagItem.load("certificates", "test")
mysql = passwords["mysql"]
Chef::Log.warn("warn: The mysql password is: '#{mysql}'")
Chef::Log.info("info: The mysql password is: '#{mysql}'")
